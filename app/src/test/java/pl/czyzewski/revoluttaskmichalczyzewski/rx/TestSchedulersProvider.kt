package pl.czyzewski.revoluttaskmichalczyzewski.rx

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler

object TrampolineSchedulersProvider : SchedulersProvider {
    override val main: Scheduler = Schedulers.trampoline()
    override val computation: Scheduler = Schedulers.trampoline()
    override val io: Scheduler = Schedulers.trampoline()
}

object TestSchedulersProvider : SchedulersProvider {
    val testScheduler = TestScheduler()

    override val main: Scheduler = Schedulers.trampoline()
    override val computation: Scheduler = testScheduler
    override val io: Scheduler = testScheduler
}
