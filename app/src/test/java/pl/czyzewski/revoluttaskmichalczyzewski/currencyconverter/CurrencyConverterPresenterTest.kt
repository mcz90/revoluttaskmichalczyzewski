package pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter

import com.nhaarman.mockitokotlin2.*
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subjects.PublishSubject
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import pl.czyzewski.revoluttaskmichalczyzewski.R
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.CurrencyRatesMapper.CurrencyConverterState
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.ICurrencyRatesMapper
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.usecase.GetCurrencyRatesUseCase.CurrencyConverterData
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.usecase.IGetCurrencyRatesUseCase
import pl.czyzewski.revoluttaskmichalczyzewski.model.CurrencyRateModel
import pl.czyzewski.revoluttaskmichalczyzewski.network.*
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.CurrenciesRatesDto
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency.*
import pl.czyzewski.revoluttaskmichalczyzewski.rx.TrampolineSchedulersProvider

class CurrencyConverterPresenterTest {

    private lateinit var presenter: CurrencyConverterPresenter

    private var view: ICurrencyConverterView = mock()
    private var getCurrencyRatesUseCase: IGetCurrencyRatesUseCase = mock()
    private var currencyRatesMapper: ICurrencyRatesMapper = mock()
    private val schedulersProvider = TrampolineSchedulersProvider
    private lateinit var currenciesRatesDto: CurrenciesRatesDto
    private lateinit var currenciesRatesModels: List<CurrencyRateModel>

    private val refreshClicks = PublishSubject.create<Unit>()
    private val inputTextChanged = PublishSubject.create<String>()
    private val currencyViewClicks = PublishSubject.create<Int>()
    private val currencyMoved = PublishSubject.create<Currency>()


    data class CurrencyConverterTest(
        val dto: CurrenciesRatesDto,
        val resultModel: List<CurrencyRateModel>? = null,
        val isErrorTest: Boolean? = false,
        val errorStringRes: Int? = null,
        val throwable: Throwable? = null
    )

    data class CurrencyConverterInteractionsTest(
        val refreshClick: Boolean? = false,
        val currencyClick: Boolean? = false,
        val clickedCurrencyPosition: Int? = null,
        val currencyMoved: Boolean? = false,
        val inputTextChanged: Boolean? = false,
        val inputText: String? = null,
        val expectedCurrencyRate: Float? = null
    )


    @Before
    fun setUp() {
        currenciesRatesDto = CurrenciesRatesDto(
            base = EUR,
            date = "",
            rates = mapOf(
                EUR to 1F,
                PLN to 1F,
                THB to 1F,
                AUD to 1F,
                USD to 1F
            )
        )

        currenciesRatesModels = listOf(
            CurrencyRateModel(currency = EUR, currencyValue = 1F, flagUrl = "url"),
            CurrencyRateModel(currency = PLN, currencyValue = 1F, flagUrl = "url"),
            CurrencyRateModel(currency = THB, currencyValue = 1F, flagUrl = "url"),
            CurrencyRateModel(currency = AUD, currencyValue = 1F, flagUrl = "url"),
            CurrencyRateModel(currency = USD, currencyValue = 1F, flagUrl = "url")
        )

        presenter = CurrencyConverterPresenter(
            view,
            getCurrencyRatesUseCase,
            currencyRatesMapper,
            schedulersProvider
        )
    }

    @Test
    fun `screen created test`() {
        whenever(getCurrencyRatesUseCase.getCurrencyRatesContinuously(any()))
            .thenReturn(Observable.just(CurrencyConverterData.Loaded(currenciesRatesDto)))

        whenever(currencyRatesMapper.map(any(), ArgumentMatchers.anyFloat()))
            .thenReturn(CurrencyConverterState.Loaded(currenciesRatesModels, 1F))

        whenever(view.inputTextChanged()).thenReturn(inputTextChanged)

        presenter.onCreate()

        verify(view).prepareRecyclerView()
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `screen started test`() = test(
        CurrencyConverterTest(
            dto = currenciesRatesDto,
            resultModel = currenciesRatesModels
        )
    )

    @Test
    fun `screen started server error test`() = test(
        CurrencyConverterTest(
            dto = currenciesRatesDto,
            isErrorTest = true,
            throwable = ServerError(),
            errorStringRes = R.string.error_server
        )
    )

    @Test
    fun `screen started client error test`() = test(
        CurrencyConverterTest(
            dto = currenciesRatesDto,
            isErrorTest = true,
            throwable = ClientError(),
            errorStringRes = R.string.error_client
        )
    )

    @Test
    fun `screen started no internet error test`() = test(
        CurrencyConverterTest(
            dto = currenciesRatesDto,
            isErrorTest = true,
            throwable = NoInternetError(),
            errorStringRes = R.string.error_no_internet
        )
    )

    @Test
    fun `screen started timeout error test`() = test(
        CurrencyConverterTest(
            dto = currenciesRatesDto,
            isErrorTest = true,
            throwable = TimeoutError(),
            errorStringRes = R.string.error_timeout
        )
    )

    @Test
    fun `screen started redirect error test`() = test(
        CurrencyConverterTest(
            dto = currenciesRatesDto,
            isErrorTest = true,
            throwable = UnexpectedRedirectError(),
            errorStringRes = R.string.error_unexpected_redirect
        )
    )

    @Test
    fun `screen started unhandled error test`() = test(
        CurrencyConverterTest(
            dto = currenciesRatesDto,
            isErrorTest = true,
            throwable = UnhandledError,
            errorStringRes = R.string.error_unhandled
        )
    )

    @Test
    fun `refresh click test`() = clickTest(
        CurrencyConverterInteractionsTest(
            refreshClick = true
        )
    )

    @Test
    fun `currency row click test`() = clickTest(
        CurrencyConverterInteractionsTest(
            currencyClick = true,
            clickedCurrencyPosition = 5
        )
    )

    @Test
    fun `currency currency moved test`() = clickTest(
        CurrencyConverterInteractionsTest(
            currencyMoved = true
        )
    )

    @Test
    fun `text input changed properly test `() = clickTest(
        CurrencyConverterInteractionsTest(
            inputTextChanged = true,
            inputText = "12345",
            expectedCurrencyRate = 12345F
        )
    )

    @Test
    fun `text input changed properly test 1`() = clickTest(
        CurrencyConverterInteractionsTest(
            inputTextChanged = true,
            inputText = "12345",
            expectedCurrencyRate = 12345F
        )
    )


    private fun test(test: CurrencyConverterTest) {
        when (test.isErrorTest) {
            true -> {
                whenever(getCurrencyRatesUseCase.getCurrencyRatesContinuously(any()))
                    .thenReturn(Observable.just(CurrencyConverterData.Error(test.throwable!!)))

                whenever(currencyRatesMapper.map(any(), ArgumentMatchers.anyFloat()))
                    .thenReturn(CurrencyConverterState.Error(test.throwable))
            }
            false -> {
                whenever(getCurrencyRatesUseCase.getCurrencyRatesContinuously(any()))
                    .thenReturn(Observable.just(CurrencyConverterData.Loaded(test.dto)))

                whenever(currencyRatesMapper.map(any(), ArgumentMatchers.anyFloat()))
                    .thenReturn(CurrencyConverterState.Loaded(test.resultModel!!, 1F))
            }
        }
        whenever(view.inputTextChanged()).thenReturn(inputTextChanged)
        whenever(view.currencyViewClicks()).thenReturn(currencyViewClicks)
        whenever(view.refreshClicks()).thenReturn(refreshClicks)
        whenever(view.onCurrencyMoved()).thenReturn(currencyMoved)

        presenter.onStart()

        inOrder(view) {
            verify(view).refreshClicks()
            verify(view).onCurrencyMoved()
            verify(view).currencyViewClicks()
            verify(view).inputTextChanged()
        }
        when (test.isErrorTest) {
            true -> {
                inOrder(view) {
                    verify(view).showError(test.errorStringRes!!)
                    verify(view).hideProgress()
                    verify(view).hideContent()
                }
            }
            false -> {
                inOrder(view) {
                    verify(view).hideProgress()
                    verify(view).showContent()
                    verify(view).loadView(currenciesRatesModels)
                }
            }
        }
        verifyNoMoreInteractions(view)
    }

    private fun clickTest(test: CurrencyConverterInteractionsTest) {
        whenever(getCurrencyRatesUseCase.getCurrencyRatesContinuously(any()))
            .thenReturn(Observable.just(CurrencyConverterData.Loaded(currenciesRatesDto)))
        whenever(currencyRatesMapper.map(any(), ArgumentMatchers.anyFloat()))
            .thenReturn(CurrencyConverterState.Loaded(currenciesRatesModels, 1F))

        whenever(view.inputTextChanged()).thenReturn(inputTextChanged)
        whenever(view.currencyViewClicks()).thenReturn(currencyViewClicks)
        whenever(view.refreshClicks()).thenReturn(refreshClicks)
        whenever(view.onCurrencyMoved()).thenReturn(currencyMoved)

        presenter.onStart()

        inOrder(view) {
            verify(view).refreshClicks()
            verify(view).onCurrencyMoved()
            verify(view).currencyViewClicks()
            verify(view).inputTextChanged()
        }

        when {
            test.refreshClick == true -> {
                refreshClicks.onNext(Unit)
                inOrder(view) {
                    verify(view).hideError()
                    verify(view).hideProgress()
                    verify(view).showContent()
                    verify(view).loadView(currenciesRatesModels)
                }
            }
            test.currencyMoved == true -> {
                currencyMoved.onNext(EUR)
                inOrder(view) {
                    verify(view).scrollTop()
                    verify(view).hideProgress()
                    verify(view).showContent()
                    verify(view).loadView(currenciesRatesModels)
                }
            }
            test.currencyClick == true -> {
                currencyViewClicks.onNext(test.clickedCurrencyPosition!!)
                view.moveCurrencyTop(test.clickedCurrencyPosition)
            }
            test.inputTextChanged == true -> {
                inputTextChanged.onNext(test.inputText!!)
                assertThat(presenter.currencyRate).isEqualTo(test.expectedCurrencyRate)
            }
        }
    }

}