package  pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter

import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.CurrencyRatesMapper
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.CurrencyRatesMapper.CurrencyConverterState
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.FlagUrlProvider
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.ICurrencyRatesMapper
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.usecase.GetCurrencyRatesUseCase.CurrencyConverterData
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.CurrenciesRatesDto
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency.*

class CurrencyRatesMapperTest {

    private lateinit var mapper: ICurrencyRatesMapper

    private val flagsProvider: FlagUrlProvider = mock()

    private val sortedRatesMap = mapOf(
        AUD to 1F,
        BGN to 1F,
        BRL to 1F,
        CAD to 1F,
        CHF to 1F,
        CZK to 1F,
        DKK to 0F,
        EUR to 1F,
        GBP to 1F,
        HKD to 1F,
        HRK to 1F,
        HUF to 1F,
        IDR to 1F,
        ILS to 1F,
        INR to 1F,
        ISK to 1F,
        JPY to 1F,
        KRW to 1F,
        MXN to 1F,
        MYR to 1F,
        NOK to 1F,
        NZD to 1F,
        PHP to 1F,
        PLN to 1F,
        RON to 1F,
        RUB to 1F,
        SEK to 1F,
        SGD to 1F,
        THB to 1F,
        TRY to 1F,
        USD to 1F,
        ZAR to 2F
    )

    @Before
    fun setUp() {
        mapper = CurrencyRatesMapper(
            flagsProvider
        )
    }

    @Test
    fun `maps loaded data to loaded state`() = test(
        CurrencyRatesTest(
            CurrencyConverterData.Loaded(
                currenciesRatesDto = CurrenciesRatesDto(
                    base = EUR,
                    date = "",
                    rates = mapOf(
                        AUD to 1F,
                        BGN to 1F,
                        BRL to 1F,
                        CAD to 1F,
                        CHF to 1F,
                        CZK to 1F,
                        DKK to 0F,
                        EUR to 1F,
                        GBP to 1F,
                        HKD to 1F,
                        HRK to 1F,
                        HUF to 1F,
                        IDR to 1F,
                        ILS to 1F,
                        INR to 1F,
                        ISK to 1F,
                        JPY to 1F,
                        KRW to 1F,
                        MXN to 1F,
                        MYR to 1F,
                        NOK to 1F,
                        NZD to 1F,
                        PHP to 1F,
                        PLN to 1F,
                        RON to 1F,
                        RUB to 1F,
                        SEK to 1F,
                        SGD to 1F,
                        THB to 1F,
                        TRY to 1F,
                        USD to 1F,
                        ZAR to 2F
                    )
                )
            ),
            currencyRate = 1.2336F
        )
    )

    @Test
    fun `maps and sorts loaded data to error state`() = test(
        CurrencyRatesTest(
            CurrencyConverterData.Loaded(
                currenciesRatesDto = CurrenciesRatesDto(
                    base = EUR,
                    date = "",
                    rates = mapOf(
                        ZAR to 2F,
                        AUD to 1F,
                        BGN to 1F,
                        BRL to 1F,
                        CAD to 1F,
                        CHF to 1F,
                        CNY to 1F,
                        DKK to 0F,
                        CZK to 1F,
                        GBP to 1F,
                        HKD to 1F,
                        HRK to 1F,
                        HUF to 1F,
                        IDR to 1F,
                        ILS to 1F,
                        INR to 1F,
                        ISK to 1F,
                        JPY to 1F,
                        KRW to 1F,
                        MXN to 1F,
                        PLN to 1F,
                        MYR to 1F,
                        NOK to 1F,
                        NZD to 1F,
                        PHP to 1F,
                        RON to 1F,
                        RUB to 1F,
                        SEK to 1F,
                        SGD to 1F,
                        THB to 1F,
                        TRY to 1F,
                        USD to 1F
                    )
                )
            ),
            currencyRate = 1.2336F,
            sorted = false
        )
    )

    @Test
    fun `maps loading data to loading state`() = test(
        CurrencyRatesTest(
            CurrencyConverterData.Loading,
            currencyRate = 1.2336F
        )
    )

    @Test
    fun `maps error data to error state`() = test(
        CurrencyRatesTest(
            CurrencyConverterData.Error(Throwable()),
            currencyRate = 1.2336F
        )
    )


    private fun test(data: CurrencyRatesTest) {
        when (data.currenciesRatesData) {
            is CurrencyConverterData.Loaded -> {
                val dto = data.currenciesRatesData.currenciesRatesDto
                val result = mapper.map(
                    CurrencyConverterData.Loaded(dto),
                    data.currencyRate
                )
                assertThat(result).isInstanceOf(CurrencyConverterState.Loaded::class.java)
                result as CurrencyConverterState.Loaded
                assertTrue(result.currenciesRatesModels[0].currency == dto.base)
                when (data.sorted) {
                    true -> sortedRatesMap.assertValues(result, data.currencyRate)
                    false -> dto.rates.assertValues(result, data.currencyRate)
                }

            }
            is CurrencyConverterData.Loading -> {
                val result = mapper.map(
                    CurrencyConverterData.Loading,
                    data.currencyRate
                )
                assertThat(result).isInstanceOf(CurrencyConverterState.Loading::class.java)
            }
            is CurrencyConverterData.Error -> {
                val result = mapper.map(
                    CurrencyConverterData.Error(Throwable()),
                    data.currencyRate
                )
                assertThat(result).isInstanceOf(CurrencyConverterState.Error::class.java)
            }
        }
    }

    private fun Map<Currency, Float>.assertValues(
        result: CurrencyConverterState.Loaded,
        currencyRate: Float
    ) = entries.forEachIndexed { index, entry ->
        if (index == 0) return@forEachIndexed
        val (currency, currencyValue) = entry
        assertTrue(result.currenciesRatesModels[index].currency.name == currency.name)
        assertTrue(result.currenciesRatesModels[index].currencyValue == currencyValue * currencyRate)
        assertTrue(
            result.currenciesRatesModels[index].flagUrl == flagsProvider.getFlagUrl(
                currency
            )
        )
    }


    private data class CurrencyRatesTest(
        val currenciesRatesData: CurrencyConverterData,
        val currencyRate: Float,
        val sorted: Boolean? = true
    )
}
