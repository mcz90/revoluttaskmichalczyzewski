package pl.czyzewski.revoluttaskmichalczyzewski

import android.app.Application
import pl.czyzewski.revoluttaskmichalczyzewski.di.DaggerComponents

class CurrencyConverterApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        DaggerComponents.app = this
    }

}