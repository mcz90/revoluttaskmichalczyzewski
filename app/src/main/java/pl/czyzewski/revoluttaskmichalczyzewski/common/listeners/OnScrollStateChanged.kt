package pl.czyzewski.revoluttaskmichalczyzewski.common.listeners

import androidx.recyclerview.widget.RecyclerView

class OnScrollStateChanged(private val stateChangedAction: (Int) -> Unit) :
    RecyclerView.OnScrollListener() {

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        stateChangedAction.invoke(newState)
    }
}
