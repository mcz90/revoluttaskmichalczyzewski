package pl.czyzewski.revoluttaskmichalczyzewski.common.view

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.jakewharton.rxbinding2.view.clicks
import kotlinx.android.synthetic.main.view_error.view.*
import pl.czyzewski.revoluttaskmichalczyzewski.R
import pl.czyzewski.revoluttaskmichalczyzewski.common.gone
import pl.czyzewski.revoluttaskmichalczyzewski.common.visible

class ErrorView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        inflate(context, R.layout.view_error, this)
    }

    fun show(@StringRes errorTextRes: Int) {
        error_cause.text = context.getText(errorTextRes)
        visible()
    }

    fun hide() = gone()

    fun refreshClicks() = refresh_button.clicks()

}