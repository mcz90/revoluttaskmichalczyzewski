package pl.czyzewski.revoluttaskmichalczyzewski.common

import android.view.View
import android.view.View.VISIBLE
import android.view.View.GONE

fun View.visible() {
    visibility = VISIBLE
}

fun View.gone() {
    visibility = GONE
}