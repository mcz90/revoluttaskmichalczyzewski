package pl.czyzewski.revoluttaskmichalczyzewski.model

import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency

data class CurrencyRateModel(
    val currency: Currency,
    val currencyValue: Float,
    val flagUrl: String
)