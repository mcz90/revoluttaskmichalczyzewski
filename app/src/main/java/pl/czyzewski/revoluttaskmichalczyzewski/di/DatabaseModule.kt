package pl.czyzewski.revoluttaskmichalczyzewski.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import pl.czyzewski.revoluttaskmichalczyzewski.database.AppDatabase
import pl.czyzewski.revoluttaskmichalczyzewski.database.dao.CurrencyRatesDao
import javax.inject.Singleton


@Module
class DatabaseModule(private val applicationContext: Application) {

    @Singleton
    @Provides
    fun provideAppDatabase(): AppDatabase =
        Room.databaseBuilder(applicationContext, AppDatabase::class.java, AppDatabase.DB_NAME)
            .build()

    @Provides
    fun provideCurrencyRatesDao(appDatabase: AppDatabase): CurrencyRatesDao =
        appDatabase.currencyRatesDao()
}
