package pl.czyzewski.revoluttaskmichalczyzewski.di

import android.content.Context
import dagger.Component
import dagger.Module
import dagger.Provides
import pl.czyzewski.revoluttaskmichalczyzewski.CurrencyConverterApplication

@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun context(): Context
}

@Module
class ApplicationModule(private val application: CurrencyConverterApplication) {

    @Provides
    fun provideApplicationContext(): Context = application
}
