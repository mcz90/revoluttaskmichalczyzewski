package pl.czyzewski.revoluttaskmichalczyzewski.di

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import pl.czyzewski.revoluttaskmichalczyzewski.network.ErrorMapper
import pl.czyzewski.revoluttaskmichalczyzewski.network.IErrorMapper
import pl.czyzewski.revoluttaskmichalczyzewski.network.RevolutApiService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class NetworkModule {

    companion object {
        private const val TIMEOUT_LONG = 5000L
        private const val BASE_URL = "https://revolut.duckdns.org"
    }

    @Provides
    fun provideErrorMapper(mapper: ErrorMapper): IErrorMapper = mapper

    @Singleton
    @Provides
    fun provideOkHttpClient(
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder
            .readTimeout(TIMEOUT_LONG, TimeUnit.SECONDS)
            .connectTimeout(TIMEOUT_LONG, TimeUnit.SECONDS)

        return builder.build()
    }

    @Singleton
    @Provides
    fun provideRevolutApi(client: OkHttpClient): RevolutApiService {
        val revolutApi = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BASE_URL)
            .client(client)
            .build()

        return revolutApi.create()
    }
}
