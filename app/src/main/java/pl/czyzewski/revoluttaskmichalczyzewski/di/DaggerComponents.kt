package pl.czyzewski.revoluttaskmichalczyzewski.di

import pl.czyzewski.revoluttaskmichalczyzewski.CurrencyConverterApplication

object DaggerComponents {

    lateinit var app: CurrencyConverterApplication

    private val appComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(app))
            .build()
    }

    val dataComponent: DataComponent by lazy {
        DaggerDataComponent.builder().apply {
            applicationComponent(appComponent)
            networkModule(NetworkModule())
            databaseModule(DatabaseModule(app))
            rxModule(RxModule())
        }.build()
    }
}
