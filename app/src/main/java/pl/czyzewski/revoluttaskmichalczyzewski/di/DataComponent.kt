package pl.czyzewski.revoluttaskmichalczyzewski.di

import dagger.Component
import pl.czyzewski.revoluttaskmichalczyzewski.CurrencyConverterApplication
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.dagger.CurrencyConverterComponent
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.dagger.CurrencyConverterModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [NetworkModule::class, DatabaseModule::class, RxModule::class],
    dependencies = [ApplicationComponent::class]
)
interface DataComponent {
    fun inject(application: CurrencyConverterApplication)

    fun plus(module: CurrencyConverterModule): CurrencyConverterComponent
}
