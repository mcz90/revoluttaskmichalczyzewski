package pl.czyzewski.revoluttaskmichalczyzewski.di

import dagger.Module
import dagger.Provides
import pl.czyzewski.revoluttaskmichalczyzewski.rx.DefaultSchedulersProvider
import pl.czyzewski.revoluttaskmichalczyzewski.rx.SchedulersProvider

@Module
class RxModule {

    @Provides
    fun provideSchedulersProvider(): SchedulersProvider = DefaultSchedulersProvider
}
