package pl.czyzewski.revoluttaskmichalczyzewski.network.dto

import androidx.annotation.StringRes
import pl.czyzewski.revoluttaskmichalczyzewski.R


data class CurrenciesRatesDto(
    val base: Currency,
    val date: String,
    val rates: Map<Currency, Float>
)


enum class Currency(@StringRes val currencyType: Int) {
    AUD(R.string.currency_aud),
    BGN(R.string.currency_bgn),
    BRL(R.string.currency_brl),
    CAD(R.string.currency_cad),
    CHF(R.string.currency_chf),
    CNY(R.string.currency_cny),
    CZK(R.string.currency_czk),
    DKK(R.string.currency_dkk),
    EUR(R.string.currency_eur),
    GBP(R.string.currency_gbp),
    HKD(R.string.currency_hkd),
    HRK(R.string.currency_hrk),
    HUF(R.string.currency_huf),
    IDR(R.string.currency_idr),
    ILS(R.string.currency_ils),
    INR(R.string.currency_inr),
    ISK(R.string.currency_isk),
    JPY(R.string.currency_jpy),
    KRW(R.string.currency_krw),
    MXN(R.string.currency_mxn),
    MYR(R.string.currency_myr),
    NOK(R.string.currency_nok),
    NZD(R.string.currency_nzd),
    PHP(R.string.currency_php),
    PLN(R.string.currency_pln),
    RON(R.string.currency_ron),
    RUB(R.string.currency_rub),
    SEK(R.string.currency_sek),
    SGD(R.string.currency_sgd),
    THB(R.string.currency_thb),
    TRY(R.string.currency_try),
    USD(R.string.currency_usd),
    ZAR(R.string.currency_zar)
}
