package pl.czyzewski.revoluttaskmichalczyzewski.network

import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

class ServerError(cause: Throwable? = null) : Throwable(cause)
class ClientError(cause: Throwable? = null) : Throwable(cause)
class NoInternetError(cause: Throwable? = null) : Throwable(cause)
class TimeoutError(cause: Throwable? = null) : Throwable(cause)
class UnexpectedRedirectError(cause: Throwable? = null) : Throwable(cause)
object UnhandledError : Throwable()

interface IErrorMapper {
    fun map(cause: Throwable): Throwable
}

class ErrorMapper @Inject constructor() : IErrorMapper {

    override fun map(cause: Throwable): Throwable = when (cause) {
        is HttpException -> when (cause.code()) {
            in 300..399 -> UnexpectedRedirectError(cause)
            in 400..499 -> ClientError(cause)
            in 500..599 -> ServerError(cause)
            else -> UnhandledError
        }
        is UnknownHostException -> NoInternetError(cause)
        is SocketTimeoutException -> TimeoutError(cause)
        else -> UnhandledError
    }

}
