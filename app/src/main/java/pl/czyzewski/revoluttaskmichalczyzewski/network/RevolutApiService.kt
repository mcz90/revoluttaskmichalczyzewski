package pl.czyzewski.revoluttaskmichalczyzewski.network

import io.reactivex.Observable
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.CurrenciesRatesDto
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency
import retrofit2.http.GET
import retrofit2.http.Query


interface RevolutApiService {

    @GET("/latest")
    fun getCurrencyRates(@Query("base") currency: Currency? = Currency.EUR): Observable<CurrenciesRatesDto>
}