package  pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.storage

import pl.czyzewski.revoluttaskmichalczyzewski.database.dao.CurrencyRatesDao
import pl.czyzewski.revoluttaskmichalczyzewski.database.entity.CurrencyRateEntity
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.CurrenciesRatesDto
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency
import javax.inject.Inject

interface ICurrencyRatesStorage {
    fun loadLastRates(): CurrencyRateEntity
    fun saveLastRates(dto: CurrenciesRatesDto)
}

class CurrencyRatesStorage @Inject constructor(private val currencyRatesDao: CurrencyRatesDao) :
    ICurrencyRatesStorage {

    override fun loadLastRates(): CurrencyRateEntity = currencyRatesDao.load(Currency.AUD)

    override fun saveLastRates(dto: CurrenciesRatesDto) = currencyRatesDao.insert(
        CurrencyRateEntity(
            base = dto.base,
            date = dto.date,
            rates = dto.rates
        )
    )

}
