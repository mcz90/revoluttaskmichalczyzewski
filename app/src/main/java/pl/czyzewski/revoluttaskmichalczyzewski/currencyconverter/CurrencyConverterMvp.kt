package pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter

import androidx.annotation.StringRes
import androidx.lifecycle.LifecycleObserver
import io.reactivex.Observable
import pl.czyzewski.revoluttaskmichalczyzewski.model.CurrencyRateModel
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency

interface ICurrencyConverterView {
    val layoutRes: Int
    fun prepareRecyclerView()
    fun showContent()
    fun hideContent()
    fun showProgress()
    fun hideProgress()
    fun showError(@StringRes errorTextRes: Int)
    fun hideError()
    fun loadView(currencyRates: List<CurrencyRateModel>)
    fun refreshClicks(): Observable<Unit>
    fun currencyViewClicks(): Observable<Int>
    fun inputTextChanged(): Observable<String>
    fun onCurrencyMoved(): Observable<Currency>
    fun moveCurrencyTop(position: Int)
    fun scrollTop()
    fun clear()
}

interface ICurrencyConverterPresenter : LifecycleObserver
