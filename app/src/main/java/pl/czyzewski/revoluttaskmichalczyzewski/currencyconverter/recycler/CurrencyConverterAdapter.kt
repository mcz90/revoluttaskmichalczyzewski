package pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.recycler

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.bumptech.glide.Glide
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.view_currency_recycler_item.view.*
import pl.czyzewski.revoluttaskmichalczyzewski.R
import pl.czyzewski.revoluttaskmichalczyzewski.common.listeners.TextChangedListener
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.recycler.CurrencyConverterDiffUtil.Companion.UPDATE_CURRENCY
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.recycler.CurrencyConverterDiffUtil.Companion.UPDATE_CURRENCY_VALUE
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.recycler.CurrencyConverterDiffUtil.Companion.UPDATE_FLAG_URL
import pl.czyzewski.revoluttaskmichalczyzewski.model.CurrencyRateModel
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency
import javax.inject.Inject


interface ICurrencyConverterAdapter {
    fun updateData(currencyRates: List<CurrencyRateModel>)
    fun currencyViewClicks(): Observable<Int>
    fun inputTextChanged(): Observable<String>
    fun onCurrencyMoved(): Observable<Currency>
    fun moveCurrencyTop(oldPosition: Int)
    fun clear()
}

class CurrencyConverterAdapter @Inject constructor(private val context: Context) :
    Adapter<CurrencyConverterAdapter.CurrencyConverterViewHolder>(), ICurrencyConverterAdapter {

    private val disposable = CompositeDisposable()

    private var currencyRates: MutableList<CurrencyRateModel> = mutableListOf()
    private val inputTextChangedSubject = PublishSubject.create<String>()
    private val currencyViewClickSubject = PublishSubject.create<Int>()
    private val onItemMovedSubject = PublishSubject.create<Currency>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyConverterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.view_currency_recycler_item,
            parent,
            false
        )
        return CurrencyConverterViewHolder(view)
    }

    override fun onBindViewHolder(holder: CurrencyConverterViewHolder, position: Int) {
        val model = currencyRates[position]
        when (position) {
            0 -> holder.bindBaseModel(model)
            else -> holder.bindCommonModel(model)
        }
    }

    override fun onBindViewHolder(
        holder: CurrencyConverterViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
            return
        }
        val bundle: Bundle = payloads[0] as Bundle

        bundle.updateValue(UPDATE_CURRENCY) {
            holder.updateCurrency(bundle.getString(it))
        }
        bundle.updateValue(UPDATE_CURRENCY_VALUE) {
            holder.updateCurrencyInput(bundle.getFloat(it))
        }
        bundle.updateValue(UPDATE_FLAG_URL) {
            holder.updateFlagUrl(bundle.getString(it))
        }
    }

    private fun Bundle.updateValue(key: String, updateAction: (String) -> Unit) {
        if (this.containsKey(key)) {
            updateAction.invoke(key)
        }
    }

    override fun getItemCount(): Int = currencyRates.size

    override fun updateData(currencyRates: List<CurrencyRateModel>) {
        val diffResult =
            DiffUtil.calculateDiff(CurrencyConverterDiffUtil(this.currencyRates, currencyRates))
        diffResult.dispatchUpdatesTo(this)
        this.currencyRates.clear()
        this.currencyRates.addAll(currencyRates)
    }

    override fun currencyViewClicks(): Observable<Int> = currencyViewClickSubject

    override fun inputTextChanged(): Observable<String> = inputTextChangedSubject

    override fun onCurrencyMoved(): Observable<Currency> = onItemMovedSubject

    override fun moveCurrencyTop(oldPosition: Int) = moveTop(oldPosition)

    private fun moveTop(oldPosition: Int) {
        val removedModel = currencyRates.removeAt(oldPosition)
        currencyRates.sortBy { it.currency }
        currencyRates.add(0, removedModel)
        notifyItemRangeChanged(0, currencyRates.size)
        onItemMovedSubject.onNext(currencyRates[0].currency)
    }

    override fun clear() = disposable.clear()


    inner class CurrencyConverterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val content: View = view.content
        private val flagImage: ImageView = view.flag_image
        private val currencyType: TextView = view.currency_type
        private val currencyName: TextView = view.currency_name
        private val currencyInput: EditText = view.currency_input

        private val textChangeListener = TextChangedListener { inputTextChangedSubject.onNext(it) }


        fun bindBaseModel(currencyModel: CurrencyRateModel) {
            currencyInput.apply {
                onFocusChangeListener = null
                if (!hasFocus()) {
                    requestFocus()
                }
                addTextChangedListener(textChangeListener)
                val text = currencyModel.currencyValue.toString()
                if (text != this.text.toString()) {
                    setText(text)
                }
            }
            content.setOnClickListener(null)
            currencyType.text = currencyModel.currency.name
            currencyName.text = context.getString(currencyModel.currency.currencyType)
            Glide.with(context).load(currencyModel.flagUrl).into(flagImage)
        }

        fun bindCommonModel(currencyModel: CurrencyRateModel) {
            currencyInput.apply {
                setOnFocusChangeListener { _, hasFocus ->
                    if (hasFocus) {
                        currencyViewClickSubject.onNext(layoutPosition)
                    }
                }
                removeTextChangedListener(textChangeListener)
                setText(currencyModel.currencyValue.toString())
            }
            content.setOnClickListener { currencyViewClickSubject.onNext(layoutPosition) }

            currencyType.text = currencyModel.currency.name
            currencyName.text = context.getString(currencyModel.currency.currencyType)
            Glide.with(context).load(currencyModel.flagUrl).into(flagImage)
        }


        fun updateCurrency(currencyEnumName: String?) = currencyEnumName?.let {
            val currency = Currency.valueOf(it)
            currencyType.text = currency.name
            currencyName.text = context.getString(currency.currencyType)
        }

        fun updateCurrencyInput(currencyValue: Float) =
            currencyInput.setText(currencyValue.toString())


        fun updateFlagUrl(flagUrl: String?) = flagUrl?.let {
            Glide.with(context).load(flagUrl).into(flagImage)
        }

    }
}

