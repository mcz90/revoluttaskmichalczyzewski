package pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.dagger.CurrencyConverterModule
import pl.czyzewski.revoluttaskmichalczyzewski.di.DaggerComponents
import javax.inject.Inject

class CurrencyConverterActivity : AppCompatActivity() {

    @Inject
    lateinit var view: ICurrencyConverterView

    @Inject
    lateinit var presenter: ICurrencyConverterPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DaggerComponents.dataComponent
            .plus(CurrencyConverterModule(this))
            .inject(this)

        listOf(presenter).forEach { lifecycle.addObserver(it) }
        setContentView(view.layoutRes)
    }
}
