package pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter

import android.app.Activity
import androidx.annotation.StringRes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_currencies_list.*
import pl.czyzewski.revoluttaskmichalczyzewski.R
import pl.czyzewski.revoluttaskmichalczyzewski.common.gone
import pl.czyzewski.revoluttaskmichalczyzewski.common.visible
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.recycler.CurrencyConverterAdapter
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.recycler.ICurrencyConverterAdapter
import pl.czyzewski.revoluttaskmichalczyzewski.model.CurrencyRateModel
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency
import javax.inject.Inject

class CurrencyConverterView @Inject constructor(
    private val activity: Activity,
    private val adapter: ICurrencyConverterAdapter
) : ICurrencyConverterView {

    override val layoutRes: Int
        get() = R.layout.activity_currencies_list

    private lateinit var recyclerView: RecyclerView


    override fun prepareRecyclerView() {
        recyclerView = activity.recyclerView
        recyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = this@CurrencyConverterView.adapter as CurrencyConverterAdapter
        }
    }

    override fun showContent() = recyclerView.visible()

    override fun hideContent() = recyclerView.gone()

    override fun showProgress() = activity.progress_bar.visible()

    override fun hideProgress() = activity.progress_bar.gone()

    override fun showError(@StringRes errorTextRes: Int) = activity.error_view.show(errorTextRes)

    override fun hideError() = activity.error_view.hide()

    override fun loadView(currencyRates: List<CurrencyRateModel>) =
        adapter.updateData(currencyRates)

    override fun refreshClicks(): Observable<Unit> = activity.error_view.refreshClicks()

    override fun currencyViewClicks(): Observable<Int> = adapter.currencyViewClicks()

    override fun inputTextChanged(): Observable<String> = adapter.inputTextChanged()

    override fun onCurrencyMoved(): Observable<Currency> = adapter.onCurrencyMoved()

    override fun moveCurrencyTop(position: Int) = adapter.moveCurrencyTop(position)

    override fun scrollTop() = recyclerView.scrollToPosition(0)

    override fun clear() = adapter.clear()
}
