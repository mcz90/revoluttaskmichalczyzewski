package pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.dagger

import android.app.Activity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.*
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.CurrencyRatesMapper
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.ICurrencyRatesMapper
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.recycler.CurrencyConverterAdapter
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.recycler.ICurrencyConverterAdapter
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.storage.CurrencyRatesStorage
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.storage.ICurrencyRatesStorage
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.usecase.GetCurrencyRatesUseCase
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.usecase.IGetCurrencyRatesUseCase

@Subcomponent(modules = [CurrencyConverterModule::class])
interface CurrencyConverterComponent {
    fun inject(currencyConverterActivity: CurrencyConverterActivity)
}

@Module
class CurrencyConverterModule(private var activity: Activity) {

    @Provides
    fun provideActivity(): Activity = activity

    @Provides
    fun provideView(view: CurrencyConverterView): ICurrencyConverterView = view

    @Provides
    fun provideAdapter(adapter: CurrencyConverterAdapter): ICurrencyConverterAdapter = adapter

    @Provides
    fun providePresenter(presenter: CurrencyConverterPresenter): ICurrencyConverterPresenter =
        presenter

    @Provides
    fun provideUseCase(useCase: GetCurrencyRatesUseCase): IGetCurrencyRatesUseCase = useCase

    @Provides
    fun provideMapper(mapper: CurrencyRatesMapper): ICurrencyRatesMapper = mapper

    @Provides
    fun provideStorage(storage: CurrencyRatesStorage): ICurrencyRatesStorage = storage
}
