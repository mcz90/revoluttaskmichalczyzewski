package pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.recycler

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import pl.czyzewski.revoluttaskmichalczyzewski.model.CurrencyRateModel

class CurrencyConverterDiffUtil(
    private val previousCurrencyRates: List<CurrencyRateModel>,
    private val currentCurrencyRates: List<CurrencyRateModel>
) : DiffUtil.Callback() {

    companion object {
        const val UPDATE_CURRENCY = "UPDATE_CURRENCY"
        const val UPDATE_CURRENCY_VALUE = "UPDATE_CURRENCY_VALUE"
        const val UPDATE_FLAG_URL = "UPDATE_FLAG_URL"
    }

    override fun getOldListSize(): Int = previousCurrencyRates.size

    override fun getNewListSize(): Int = currentCurrencyRates.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        previousCurrencyRates[oldItemPosition].currency == currentCurrencyRates[newItemPosition].currency

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        areModelsTheSame(oldItemPosition, newItemPosition)


    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldItem = previousCurrencyRates[oldItemPosition]
        val newItem = currentCurrencyRates[newItemPosition]

        val payload = Bundle()
        if (oldItem.currency != newItem.currency) {
            payload.putString(UPDATE_CURRENCY, newItem.currency.name)
        }
        if (oldItem.currencyValue != newItem.currencyValue) {
            payload.putSerializable(UPDATE_CURRENCY_VALUE, newItem.currencyValue)
        }
        if (oldItem.flagUrl != newItem.flagUrl) {
            payload.putSerializable(UPDATE_FLAG_URL, newItem.flagUrl)
        }
        return payload
    }


    private fun areModelsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        previousCurrencyRates[oldItemPosition].currency == currentCurrencyRates[newItemPosition].currency
                && previousCurrencyRates[oldItemPosition].currencyValue == currentCurrencyRates[newItemPosition].currencyValue
                && previousCurrencyRates[oldItemPosition].flagUrl == currentCurrencyRates[newItemPosition].flagUrl
}