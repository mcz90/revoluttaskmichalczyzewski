package pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.usecase

import io.reactivex.Observable
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.storage.ICurrencyRatesStorage
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.usecase.GetCurrencyRatesUseCase.CurrencyConverterData
import pl.czyzewski.revoluttaskmichalczyzewski.network.IErrorMapper
import pl.czyzewski.revoluttaskmichalczyzewski.network.RevolutApiService
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.CurrenciesRatesDto
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency
import pl.czyzewski.revoluttaskmichalczyzewski.rx.SchedulersProvider
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface IGetCurrencyRatesUseCase {
    fun getCurrencyRatesContinuously(currency: Currency? = null): Observable<CurrencyConverterData>
}

class GetCurrencyRatesUseCase @Inject constructor(
    private val revolutApiService: RevolutApiService,
    private val currencyRatingStorage: ICurrencyRatesStorage,
    private val errorMapper: IErrorMapper,
    private val schedulersProvider: SchedulersProvider
) : IGetCurrencyRatesUseCase {
    // TODO implement database related logic. Store api calls results in database,
    //  separately for every base currency. Use database instead api as single source of truth.
    //  Handle errors with snackbars if database is not empty to avoid covering the UI.

    sealed class CurrencyConverterData {
        object Loading : CurrencyConverterData()
        data class Loaded(val currenciesRatesDto: CurrenciesRatesDto) : CurrencyConverterData()
        data class Error(val throwable: Throwable) : CurrencyConverterData()
    }


    companion object {
        private const val REQUEST_INTERVAL = 1L
    }

    override fun getCurrencyRatesContinuously(currency: Currency?): Observable<CurrencyConverterData> =
        Observable.interval(REQUEST_INTERVAL, TimeUnit.SECONDS, schedulersProvider.computation)
            .flatMap { periods ->
                getCurrencyRates(currency, periods)
                    .subscribeOn(schedulersProvider.io)
                    .observeOn(schedulersProvider.main)
            }

    private fun getCurrencyRates(
        currency: Currency?,
        intervals: Long
    ): Observable<CurrencyConverterData> {
        val currencyRatesObservable = revolutApiService.getCurrencyRates(currency)
            .handleSuccess()
            .handleErrors(errorMapper)

        return if (intervals == 0L) {
            currencyRatesObservable.startWith(CurrencyConverterData.Loading)
        } else {
            currencyRatesObservable
        }
    }

    private fun Observable<CurrenciesRatesDto>.handleSuccess(): Observable<CurrencyConverterData> =
        map { CurrencyConverterData.Loaded(it) }

    private fun Observable<CurrencyConverterData>.handleErrors(mapper: IErrorMapper): Observable<CurrencyConverterData> =
        onErrorResumeNext { t: Throwable -> Observable.just(CurrencyConverterData.Error(mapper.map(t))) }
}
