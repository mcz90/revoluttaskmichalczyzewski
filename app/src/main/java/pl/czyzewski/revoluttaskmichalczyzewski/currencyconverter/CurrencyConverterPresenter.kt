package pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import pl.czyzewski.revoluttaskmichalczyzewski.R
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.CurrencyRatesMapper.CurrencyConverterState
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.ICurrencyRatesMapper
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.usecase.IGetCurrencyRatesUseCase
import pl.czyzewski.revoluttaskmichalczyzewski.network.*
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency
import pl.czyzewski.revoluttaskmichalczyzewski.rx.SchedulersProvider
import javax.inject.Inject

class CurrencyConverterPresenter @Inject constructor(
    private val view: ICurrencyConverterView,
    private val getCurrencyRatesUseCase: IGetCurrencyRatesUseCase,
    private val currencyRatesMapper: ICurrencyRatesMapper,
    private val schedulersProvider: SchedulersProvider
) : ICurrencyConverterPresenter {

    private val userInteractionsSubscriptions = CompositeDisposable()
    private val currencyRatesSubscription = CompositeDisposable()

    @VisibleForTesting
    var currencyRate: Float = 0F

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        view.prepareRecyclerView()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        userInteractionsSubscriptions += view.refreshClicks()
            .subscribe {
                view.hideError()
                subscribeCurrencyRates(Currency.EUR)
            }

        userInteractionsSubscriptions += view.onCurrencyMoved()
            .subscribe { currency ->
                view.scrollTop()
                subscribeCurrencyRates(currency)
            }

        userInteractionsSubscriptions += view.currencyViewClicks()
            .subscribe { position ->
                currencyRatesSubscription.clear()
                view.moveCurrencyTop(position)
            }

        userInteractionsSubscriptions += view.inputTextChanged()
            .distinctUntilChanged()
            .subscribe { currencyRate = it.takeIf { it.isNullOrEmpty().not() }?.toFloat() ?: 0F }

        subscribeCurrencyRates(Currency.EUR)
    }

    private fun subscribeCurrencyRates(currency: Currency?) {
        currencyRatesSubscription += getCurrencyRatesDisposable(currency)
    }

    private fun getCurrencyRatesDisposable(currency: Currency?): Disposable =
        getCurrencyRatesUseCase.getCurrencyRatesContinuously(currency)
            .map { currencyRatesMapper.map(it, currencyRate) }
            .subscribeOn(schedulersProvider.io)
            .observeOn(schedulersProvider.main)
            .subscribe(::handleSuccess, ::handleError)

    private fun handleSuccess(dataResult: CurrencyConverterState) = when (dataResult) {
        CurrencyConverterState.Loading -> view.showProgress()
        is CurrencyConverterState.Loaded -> with(view) {
            hideProgress()
            showContent()
            loadView(dataResult.currenciesRatesModels)
        }
        is CurrencyConverterState.Error -> handleError(dataResult.throwable)
    }

    private fun handleError(throwable: Throwable) {
        view.showError(
            when (throwable) {
                is ServerError -> R.string.error_server
                is ClientError -> R.string.error_client
                is NoInternetError -> R.string.error_no_internet
                is TimeoutError -> R.string.error_timeout
                is UnexpectedRedirectError -> R.string.error_unexpected_redirect
                UnhandledError -> R.string.error_unhandled
                else -> R.string.error_unhandled
            }
        )
        view.hideProgress()
        view.hideContent()
        currencyRatesSubscription.clear()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        currencyRatesSubscription.clear()
        userInteractionsSubscriptions.clear()
        view.clear()
    }

}
