package pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper

import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.mapper.CurrencyRatesMapper.CurrencyConverterState
import pl.czyzewski.revoluttaskmichalczyzewski.currencyconverter.usecase.GetCurrencyRatesUseCase.CurrencyConverterData
import pl.czyzewski.revoluttaskmichalczyzewski.model.CurrencyRateModel
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency.*
import javax.inject.Inject

interface ICurrencyRatesMapper {
    fun map(dto: CurrencyConverterData, currencyRate: Float): CurrencyConverterState
}


class CurrencyRatesMapper @Inject constructor(
    private val flagUrlProvider: FlagUrlProvider
) : ICurrencyRatesMapper {

    sealed class CurrencyConverterState {
        object Loading : CurrencyConverterState()
        data class Loaded(
            val currenciesRatesModels: List<CurrencyRateModel>,
            val currencyRate: Float
        ) : CurrencyConverterState()

        data class Error(val throwable: Throwable) : CurrencyConverterState()
    }

    override fun map(dto: CurrencyConverterData, currencyRate: Float): CurrencyConverterState =
        when (dto) {
            CurrencyConverterData.Loading -> CurrencyConverterState.Loading
            is CurrencyConverterData.Loaded -> mapLoaded(dto, currencyRate)
            is CurrencyConverterData.Error -> CurrencyConverterState.Error(dto.throwable)
        }

    private fun mapLoaded(
        dto: CurrencyConverterData.Loaded,
        currencyRate: Float
    ): CurrencyConverterState.Loaded {

        val list = mutableListOf<CurrencyRateModel>()
        val base = CurrencyRateModel(
            currency = dto.currenciesRatesDto.base,
            currencyValue = currencyRate,
            flagUrl = flagUrlProvider.getFlagUrl(dto.currenciesRatesDto.base)
        )

        val ratesList = mutableListOf<CurrencyRateModel>()
        dto.currenciesRatesDto.rates.entries
            .forEach { rate ->
                ratesList.add(
                    CurrencyRateModel(
                        currency = rate.key,
                        currencyValue = currencyRate * rate.value,
                        flagUrl = flagUrlProvider.getFlagUrl(rate.key)
                    )
                )
            }
        ratesList.sortBy { it.currency }

        list.add(base)
        list.addAll(ratesList)
        return CurrencyConverterState.Loaded(list.toList(), currencyRate)
    }
}


open class FlagUrlProvider @Inject constructor() {

    fun getFlagUrl(currency: Currency): String = when (currency) {
        AUD -> "https://www.countryflags.io/AU/shiny/64.png"
        BGN -> "https://www.countryflags.io/BG/shiny/64.png"
        BRL -> "https://www.countryflags.io/BR/shiny/64.png"
        CAD -> "https://www.countryflags.io/CA/shiny/64.png"
        CHF -> "https://www.countryflags.io/CH/shiny/64.png"
        CNY -> "https://www.countryflags.io/CN/shiny/64.png"
        CZK -> "https://www.countryflags.io/CZ/shiny/64.png"
        DKK -> "https://www.countryflags.io/DK/shiny/64.png"
        EUR -> "https://www.countryflags.io/EU/shiny/64.png"
        GBP -> "https://www.countryflags.io/GB/shiny/64.png"
        HKD -> "https://www.countryflags.io/HK/shiny/64.png"
        HRK -> "https://www.countryflags.io/HR/shiny/64.png"
        HUF -> "https://www.countryflags.io/HU/shiny/64.png"
        IDR -> "https://www.countryflags.io/ID/shiny/64.png"
        ILS -> "https://www.countryflags.io/IL/shiny/64.png"
        INR -> "https://www.countryflags.io/IN/shiny/64.png"
        ISK -> "https://www.countryflags.io/IS/shiny/64.png"
        JPY -> "https://www.countryflags.io/JP/shiny/64.png"
        KRW -> "https://www.countryflags.io/KR/shiny/64.png"
        MXN -> "https://www.countryflags.io/MX/shiny/64.png"
        MYR -> "https://www.countryflags.io/MY/shiny/64.png"
        NOK -> "https://www.countryflags.io/NO/shiny/64.png"
        NZD -> "https://www.countryflags.io/NZ/shiny/64.png"
        PHP -> "https://www.countryflags.io/PH/shiny/64.png"
        PLN -> "https://www.countryflags.io/PL/shiny/64.png"
        RON -> "https://www.countryflags.io/RO/shiny/64.png"
        RUB -> "https://www.countryflags.io/RU/shiny/64.png"
        SEK -> "https://www.countryflags.io/SE/shiny/64.png"
        SGD -> "https://www.countryflags.io/SG/shiny/64.png"
        THB -> "https://www.countryflags.io/TH/shiny/64.png"
        TRY -> "https://www.countryflags.io/TR/shiny/64.png"
        USD -> "https://www.countryflags.io/US/shiny/64.png"
        ZAR -> "https://www.countryflags.io/ZA/shiny/64.png"
    }
}
