package pl.czyzewski.revoluttaskmichalczyzewski.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency

@Entity(tableName = "lastCurrencyRates")
data class CurrencyRateEntity(
    @PrimaryKey
    val base: Currency,
    val date: String,
    val rates: Map<Currency, Float>
)
