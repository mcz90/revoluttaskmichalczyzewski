package pl.czyzewski.revoluttaskmichalczyzewski.database.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency

class CurrencyToFloatConverter {

    private val gson = Gson()

    @TypeConverter
    fun toMap(data: String): Map<Currency, Float> {
        return gson.fromJson(data, object : TypeToken<Map<Currency, Float>>() {}.type)
    }

    @TypeConverter
    fun toJsonString(map: Map<Currency, Float>): String {
        return gson.toJson(map)
    }
}
