package pl.czyzewski.revoluttaskmichalczyzewski.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import pl.czyzewski.revoluttaskmichalczyzewski.database.AppDatabase.Companion.DB_VERSION
import pl.czyzewski.revoluttaskmichalczyzewski.database.converter.CurrencyConverter
import pl.czyzewski.revoluttaskmichalczyzewski.database.converter.CurrencyToFloatConverter
import pl.czyzewski.revoluttaskmichalczyzewski.database.dao.CurrencyRatesDao
import pl.czyzewski.revoluttaskmichalczyzewski.database.entity.CurrencyRateEntity

@Database(
    entities = [CurrencyRateEntity::class],
    version = DB_VERSION,
    exportSchema = false
)
@TypeConverters(
    CurrencyConverter::class,
    CurrencyToFloatConverter::class
)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val DB_NAME = "currencies_database.db"
        const val DB_VERSION = 1
    }

    abstract fun currencyRatesDao(): CurrencyRatesDao

}
