package pl.czyzewski.revoluttaskmichalczyzewski.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import pl.czyzewski.revoluttaskmichalczyzewski.database.entity.CurrencyRateEntity
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency

@Dao
interface CurrencyRatesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: CurrencyRateEntity)

    @Query("SELECT * FROM lastCurrencyRates WHERE 'currency' == :currency")
    fun load(currency: Currency): CurrencyRateEntity
}
