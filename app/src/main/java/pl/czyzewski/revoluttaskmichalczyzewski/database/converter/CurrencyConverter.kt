package pl.czyzewski.revoluttaskmichalczyzewski.database.converter

import androidx.room.TypeConverter
import pl.czyzewski.revoluttaskmichalczyzewski.network.dto.Currency

class CurrencyConverter {

    @TypeConverter
    fun toCurrency(name: String): Currency {
        return Currency.valueOf(name)
    }

    @TypeConverter
    fun toName(currency: Currency): String {
        return currency.name
    }
}
