package pl.czyzewski.revoluttaskmichalczyzewski.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface SchedulersProvider {
    val main: Scheduler
    val computation: Scheduler
    val io: Scheduler
}

object DefaultSchedulersProvider : SchedulersProvider {
    override val main: Scheduler = AndroidSchedulers.mainThread()
    override val computation: Scheduler = Schedulers.computation()
    override val io: Scheduler = Schedulers.io()
}
